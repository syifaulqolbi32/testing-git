// const fs = require('fs');
// const option = {encoding : "utf-8"};

// const callback = (err, data)=>{

//     if(err){
//         console.log(err)
//     }

//     console.log("isi data :", data);

// }

// const benar = false;

// const janji = new Promise((resolve, reject)=> {
//     if(benar){
//         resolve("Janji terpenuhi");
//     }else {
//         reject("ingkar janji");
//     }
// });

// janji.then(data=> console.log(data)).catch(err=>console.log(err))

// function cekUmur(umur){
//     return new Promise((resolve, reject) =>{
//         console.log('Umur :', umur)

//         if (umur != '21'){
//             return reject("Umur yang dimasukkan tidak cocok")
//         }

//         resolve("Umur sesuai")
//     })
// }

// cekUmur('21')
// .then(data => console.log(data))
// .catch(err=> console.log(err))

let punyaLaptop = false;

function beliLaptop(){
    return new Promise(resolve=>{
        setTimeout(() =>{
            punyaLaptop = true;
            resolve();
        }, 1000);
    });
}

function tampilLaptop(merk){
    return new Promise((resolve, reject)=>{
        if(!punyaLaptop){
            return reject("Beli laptop dulu sanaa");
        }

        resolve("Sudah beli nih bos, merk nya ini " + merk);
    });
}

async function main(){
    await beliLaptop()
    let tampil = await tampilLaptop("Laptop Anak Bangsa");
    console.log(tampil)
}